package com.people10.hotelbooking.controller;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.people10.hotelbooking.entity.CustomerEntity;
import com.people10.hotelbooking.reposotory.CustomerDetailsReposotory;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Date;
import java.util.Optional;
import java.util.regex.Matcher;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@RunWith(SpringJUnit4ClassRunner.class)
public class CustomerDetailsControllerTest {

    private MockMvc mockMvc;
    @InjectMocks
    private CustomerDetailsController customerDetailsController;
    @Mock
    private CustomerDetailsReposotory customerDetailsReposotory;
    @Before
    public void setUp() throws Exception{
        mockMvc= MockMvcBuilders.standaloneSetup(customerDetailsController)
                .build();
    }

    @Test
    public void getCustomerDetailsByIdTest() throws Exception {
        when(customerDetailsReposotory.findById(1l)).thenReturn(Optional.of(getCustomerDetails()));
        mockMvc.perform(
                MockMvcRequestBuilders.get("/api/customer/1")
        )
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(jsonPath("$.id", Matchers.is(getCustomerDetails().getId().intValue())))
                .andExpect(jsonPath("$.firstName", Matchers.is(getCustomerDetails().getFirstName())))
                .andExpect(jsonPath("$.lastName", Matchers.is("Behera")))
                .andExpect(jsonPath("$.email", Matchers.is(getCustomerDetails().getEmail())))
                .andExpect(jsonPath("$.password", Matchers.is(getCustomerDetails().getPassword())));
    }

@Test
public void saveCustomerDetailsTest(){
    try {
        mockMvc.perform(
                MockMvcRequestBuilders.post("/api/customer/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json())
        )
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(jsonPath("$.id", Matchers.is("Your Room has been booked,ReferenceId is::"+getCustomerDetails().getId().intValue())));
    } catch (Exception e) {
        e.printStackTrace();
    }

}
    private CustomerEntity getCustomerDetails(){
        CustomerEntity customerEntity=new CustomerEntity();
        customerEntity.setId(1l);
        customerEntity.setEmail("Bhabani@gmail.com");
        customerEntity.setFirstName("Bhabani");
        customerEntity.setLastName("Behera");
        return customerEntity;
    }

    private String json() {
        CustomerEntity customerEntity=new CustomerEntity();
        customerEntity.setLastName("Bhabani");
        customerEntity.setFirstName("Behera");
        customerEntity.setEmail("bhabani@gmail.com");
        customerEntity.setPassword("Bhabani123");
        ObjectMapper mapper = new ObjectMapper();
        try {
            String jsonValue=mapper.writeValueAsString(customerEntity);
            return jsonValue;
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
    }


}
