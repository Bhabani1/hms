package com.people10.hotelbooking.boot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/*
	@Author Behera Bhabani
	This is the Root class of the Application
 */
@SpringBootApplication
@ComponentScan(basePackages = "com.*")
@EnableJpaRepositories("com.*")
@EntityScan(basePackages = "com.*")
public class HotelbookingApplication {

	public static void main(String[] args) {
		SpringApplication.run(HotelbookingApplication.class, args);
	}

}
