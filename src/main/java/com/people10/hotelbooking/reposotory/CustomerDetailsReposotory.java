package com.people10.hotelbooking.reposotory;
import com.people10.hotelbooking.entity.CustomerEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomerDetailsReposotory extends CrudRepository<CustomerEntity, Long> {
    /*@Query(value = "select c.email from customers c where c.email = ':email'", nativeQuery = true)
    public CustomerEntity findByEmailId(@Param("email") String email);*/

    public CustomerEntity save(CustomerEntity customerEntity);

}
