package com.people10.hotelbooking.controller;
import com.people10.hotelbooking.entity.CustomerEntity;
import com.people10.hotelbooking.exception.CustomerNotFoundException;
import com.people10.hotelbooking.reposotory.CustomerDetailsReposotory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.Optional;
/*
    @Auther Behera Bhabani
    @Date 05/06/2020
    This is a controller class it handle all the request and calling the Reposotory and get Value.
 */
@RestController
@RequestMapping("/api")
public class CustomerDetailsController {
    @Autowired
   private CustomerDetailsReposotory customerDetailsReposotory;

    @PostMapping("/customer")
    public ResponseEntity saveCustomerDetails(@RequestBody @Valid CustomerEntity payload){
        CustomerEntity response= customerDetailsReposotory.save(payload);
        Long customerId=response.getId();
        return ResponseEntity.status(HttpStatus.CREATED).body("Your Room has been booked,ReferenceId is::"+customerId);
    }
    @GetMapping(value = "/customer/{id}")
    public ResponseEntity<Optional<CustomerEntity>> getCustomerDetailsById(@PathVariable("id") Long id){
        Optional<CustomerEntity> customerDetails=customerDetailsReposotory.findById(id);
        if(customerDetails.isPresent()==false){
            throw new CustomerNotFoundException("UserName Not Found Plz Try Valid UserName");
        }

       return ResponseEntity.ok().body(customerDetails);

    }

}
