package com.people10.hotelbooking.entity;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "customers")
@Data
public class CustomerEntity implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(name = "FIRST_NAME")
    private String firstName;
    @Column(name = "LAST_NAME")
    private String lastName;
    @Column(name = "DOB")
    private Date dob;
    @Column(name = "EMAIL")
    private String email;
    @Column(name = "PASSWORD")
    @Size(min = 8,max = 10,message = "Password should be 8 to 10 Character")
    private String password;

}
